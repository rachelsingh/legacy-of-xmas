# Converted with LuaMapConverter, version 2012-12-05
# https://github.com/Moosader/tools/tree/master/LuaMapConverter

tileset    ../graphics/InnerTilesetWIP.png

Layer Bottom
tile_begin 1          x 0          y 0          z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 2          x 20         y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 3          x 40         y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 4          x 60         y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 5          x 80         y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 6          x 100        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 7          x 120        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 8          x 140        y 0          z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 9          x 160        y 0          z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 10         x 180        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 11         x 200        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 12         x 220        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 13         x 240        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 14         x 260        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 15         x 280        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 16         x 300        y 0          z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 17         x 0          y 20         z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 18         x 20         y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 19         x 40         y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 20         x 60         y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 21         x 80         y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 22         x 100        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 23         x 120        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 24         x 140        y 20         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 25         x 160        y 20         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 26         x 180        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 27         x 200        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 28         x 220        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 29         x 240        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 30         x 260        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 31         x 280        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 32         x 300        y 20         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 33         x 0          y 40         z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 34         x 20         y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 35         x 40         y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 36         x 60         y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 37         x 80         y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 38         x 100        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 39         x 120        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 40         x 140        y 40         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 41         x 160        y 40         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 42         x 180        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 43         x 200        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 44         x 220        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 45         x 240        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 46         x 260        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 47         x 280        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 48         x 300        y 40         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 49         x 0          y 60         z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 50         x 20         y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 51         x 40         y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 52         x 60         y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 53         x 80         y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 54         x 100        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 55         x 120        y 60         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 56         x 140        y 60         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 57         x 160        y 60         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 58         x 180        y 60         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 59         x 200        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 60         x 220        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 61         x 240        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 62         x 260        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 63         x 280        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 64         x 300        y 60         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 65         x 0          y 80         z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 66         x 20         y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 67         x 40         y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 68         x 60         y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 69         x 80         y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 70         x 100        y 80         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 71         x 120        y 80         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 72         x 140        y 80         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 73         x 160        y 80         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 74         x 180        y 80         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 75         x 200        y 80         z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 76         x 220        y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 77         x 240        y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 78         x 260        y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 79         x 280        y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 80         x 300        y 80         z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 81         x 0          y 100        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 82         x 20         y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 83         x 40         y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 84         x 60         y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 85         x 80         y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 86         x 100        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 87         x 120        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 88         x 140        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 89         x 160        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 90         x 180        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 91         x 200        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 92         x 220        y 100        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 93         x 240        y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 94         x 260        y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 95         x 280        y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 96         x 300        y 100        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 97         x 0          y 120        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 98         x 20         y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 99         x 40         y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 100        x 60         y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 101        x 80         y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 102        x 100        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 103        x 120        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 104        x 140        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 105        x 160        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 106        x 180        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 107        x 200        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 108        x 220        y 120        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 109        x 240        y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 110        x 260        y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 111        x 280        y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 112        x 300        y 120        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 113        x 0          y 140        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 114        x 20         y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 115        x 40         y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 116        x 60         y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 117        x 80         y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 118        x 100        y 140        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 119        x 120        y 140        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 120        x 140        y 140        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 121        x 160        y 140        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 122        x 180        y 140        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 123        x 200        y 140        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 124        x 220        y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 125        x 240        y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 126        x 260        y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 127        x 280        y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 128        x 300        y 140        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 129        x 0          y 160        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 130        x 20         y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 131        x 40         y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 132        x 60         y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 133        x 80         y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 134        x 100        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 135        x 120        y 160        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 136        x 140        y 160        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 137        x 160        y 160        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 138        x 180        y 160        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 139        x 200        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 140        x 220        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 141        x 240        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 142        x 260        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 143        x 280        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 144        x 300        y 160        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 145        x 0          y 180        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 146        x 20         y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 147        x 40         y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 148        x 60         y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 149        x 80         y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 150        x 100        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 151        x 120        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 152        x 140        y 180        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 153        x 160        y 180        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 154        x 180        y 180        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 155        x 200        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 156        x 220        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 157        x 240        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 158        x 260        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 159        x 280        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 160        x 300        y 180        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 161        x 0          y 200        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 162        x 20         y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 163        x 40         y 200        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 164        x 60         y 200        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 165        x 80         y 200        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 166        x 100        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 167        x 120        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 168        x 140        y 200        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 169        x 160        y 200        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 170        x 180        y 200        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 171        x 200        y 200        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 172        x 220        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 173        x 240        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 174        x 260        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 175        x 280        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 176        x 300        y 200        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 177        x 0          y 220        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 178        x 20         y 220        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 179        x 40         y 220        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 180        x 60         y 220        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 181        x 80         y 220        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 182        x 100        y 220        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 183        x 120        y 220        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 184        x 140        y 220        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 185        x 160        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 186        x 180        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 187        x 200        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 188        x 220        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 189        x 240        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 190        x 260        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 191        x 280        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 192        x 300        y 220        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 193        x 0          y 240        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 194        x 20         y 240        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 195        x 40         y 240        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 196        x 60         y 240        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 197        x 80         y 240        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 198        x 100        y 240        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 199        x 120        y 240        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 200        x 140        y 240        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 201        x 160        y 240        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 202        x 180        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 203        x 200        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 204        x 220        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 205        x 240        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 206        x 260        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 207        x 280        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 208        x 300        y 240        z 1          width 20         height 20         film_x 160        film_y 0          tile_end
tile_begin 209        x 0          y 260        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 210        x 20         y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 211        x 40         y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 212        x 60         y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 213        x 80         y 260        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 214        x 100        y 260        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 215        x 120        y 260        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 216        x 140        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 217        x 160        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 218        x 180        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 219        x 200        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 220        x 220        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 221        x 240        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 222        x 260        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 223        x 280        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 224        x 300        y 260        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 225        x 0          y 280        z 1          width 20         height 20         film_x 320        film_y 80         tile_end
tile_begin 226        x 20         y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 227        x 40         y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 228        x 60         y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 229        x 80         y 280        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 230        x 100        y 280        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 231        x 120        y 280        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 232        x 140        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 233        x 160        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 234        x 180        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 235        x 200        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 236        x 220        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 237        x 240        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 238        x 260        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 239        x 280        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 240        x 300        y 280        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 241        x 0          y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 242        x 20         y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 243        x 40         y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 244        x 60         y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 245        x 80         y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 246        x 100        y 300        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 247        x 120        y 300        z 1          width 20         height 20         film_x 260        film_y 20         tile_end
tile_begin 248        x 140        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 249        x 160        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 250        x 180        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 251        x 200        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 252        x 220        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 253        x 240        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 254        x 260        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 255        x 280        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end
tile_begin 256        x 300        y 300        z 1          width 20         height 20         film_x 140        film_y 0          tile_end


Layer Middle
tile_begin 257        x 140        y 0          z 2          width 20         height 20         film_x 180        film_y 80         tile_end
tile_begin 258        x 160        y 0          z 2          width 20         height 20         film_x 220        film_y 80         tile_end
tile_begin 259        x 140        y 20         z 2          width 20         height 20         film_x 180        film_y 80         tile_end
tile_begin 260        x 160        y 20         z 2          width 20         height 20         film_x 220        film_y 80         tile_end
tile_begin 261        x 140        y 40         z 2          width 20         height 20         film_x 180        film_y 80         tile_end
tile_begin 262        x 160        y 40         z 2          width 20         height 20         film_x 220        film_y 80         tile_end
tile_begin 263        x 60         y 60         z 2          width 20         height 20         film_x 220        film_y 120        tile_end
tile_begin 264        x 120        y 60         z 2          width 20         height 20         film_x 180        film_y 60         tile_end
tile_begin 265        x 140        y 60         z 2          width 20         height 20         film_x 260        film_y 100        tile_end
tile_begin 266        x 160        y 60         z 2          width 20         height 20         film_x 240        film_y 100        tile_end
tile_begin 267        x 180        y 60         z 2          width 20         height 20         film_x 220        film_y 60         tile_end
tile_begin 268        x 100        y 80         z 2          width 20         height 20         film_x 180        film_y 60         tile_end
tile_begin 269        x 120        y 80         z 2          width 20         height 20         film_x 260        film_y 100        tile_end
tile_begin 270        x 180        y 80         z 2          width 20         height 20         film_x 240        film_y 100        tile_end
tile_begin 271        x 200        y 80         z 2          width 20         height 20         film_x 220        film_y 60         tile_end
tile_begin 272        x 20         y 100        z 2          width 20         height 20         film_x 200        film_y 120        tile_end
tile_begin 273        x 80         y 100        z 2          width 20         height 20         film_x 180        film_y 60         tile_end
tile_begin 274        x 100        y 100        z 2          width 20         height 20         film_x 260        film_y 100        tile_end
tile_begin 275        x 140        y 100        z 2          width 20         height 20         film_x 300        film_y 260        tile_end
tile_begin 276        x 160        y 100        z 2          width 20         height 20         film_x 320        film_y 260        tile_end
tile_begin 277        x 200        y 100        z 2          width 20         height 20         film_x 240        film_y 100        tile_end
tile_begin 278        x 220        y 100        z 2          width 20         height 20         film_x 220        film_y 60         tile_end
tile_begin 279        x 40         y 120        z 2          width 20         height 20         film_x 280        film_y 140        tile_end
tile_begin 280        x 60         y 120        z 2          width 20         height 20         film_x 300        film_y 140        tile_end
tile_begin 281        x 80         y 120        z 2          width 20         height 20         film_x 180        film_y 100        tile_end
tile_begin 282        x 100        y 120        z 2          width 20         height 20         film_x 260        film_y 80         tile_end
tile_begin 283        x 140        y 120        z 2          width 20         height 20         film_x 300        film_y 280        tile_end
tile_begin 284        x 160        y 120        z 2          width 20         height 20         film_x 320        film_y 280        tile_end
tile_begin 285        x 200        y 120        z 2          width 20         height 20         film_x 240        film_y 80         tile_end
tile_begin 286        x 220        y 120        z 2          width 20         height 20         film_x 220        film_y 100        tile_end
tile_begin 287        x 80         y 140        z 2          width 20         height 20         film_x 200        film_y 120        tile_end
tile_begin 288        x 100        y 140        z 2          width 20         height 20         film_x 180        film_y 100        tile_end
tile_begin 289        x 120        y 140        z 2          width 20         height 20         film_x 260        film_y 80         tile_end
tile_begin 290        x 180        y 140        z 2          width 20         height 20         film_x 240        film_y 80         tile_end
tile_begin 291        x 200        y 140        z 2          width 20         height 20         film_x 220        film_y 100        tile_end
tile_begin 292        x 120        y 160        z 2          width 20         height 20         film_x 180        film_y 100        tile_end
tile_begin 293        x 140        y 160        z 2          width 20         height 20         film_x 260        film_y 80         tile_end
tile_begin 294        x 180        y 160        z 2          width 20         height 20         film_x 220        film_y 100        tile_end
tile_begin 295        x 300        y 160        z 2          width 20         height 20         film_x 220        film_y 120        tile_end
tile_begin 296        x 20         y 180        z 2          width 20         height 20         film_x 240        film_y 0          tile_end
tile_begin 297        x 40         y 180        z 2          width 20         height 20         film_x 260        film_y 0          tile_end
tile_begin 298        x 60         y 180        z 2          width 20         height 20         film_x 260        film_y 0          tile_end
tile_begin 299        x 80         y 180        z 2          width 20         height 20         film_x 280        film_y 0          tile_end
tile_begin 300        x 120        y 180        z 2          width 20         height 20         film_x 200        film_y 120        tile_end
tile_begin 301        x 140        y 180        z 2          width 20         height 20         film_x 180        film_y 80         tile_end
tile_begin 302        x 180        y 180        z 2          width 20         height 20         film_x 220        film_y 60         tile_end
tile_begin 303        x 240        y 180        z 2          width 20         height 20         film_x 220        film_y 120        tile_end
tile_begin 304        x 20         y 200        z 2          width 20         height 20         film_x 240        film_y 20         tile_end
tile_begin 305        x 100        y 200        z 2          width 20         height 20         film_x 260        film_y 0          tile_end
tile_begin 306        x 120        y 200        z 2          width 20         height 20         film_x 280        film_y 0          tile_end
tile_begin 307        x 140        y 200        z 2          width 20         height 20         film_x 180        film_y 100        tile_end
tile_begin 308        x 160        y 200        z 2          width 20         height 20         film_x 260        film_y 80         tile_end
tile_begin 309        x 180        y 200        z 2          width 20         height 20         film_x 240        film_y 100        tile_end
tile_begin 310        x 200        y 200        z 2          width 20         height 20         film_x 220        film_y 60         tile_end
tile_begin 311        x 20         y 220        z 2          width 20         height 20         film_x 240        film_y 40         tile_end
tile_begin 312        x 40         y 220        z 2          width 20         height 20         film_x 260        film_y 40         tile_end
tile_begin 313        x 120        y 220        z 2          width 20         height 20         film_x 280        film_y 20         tile_end
tile_begin 314        x 160        y 220        z 2          width 20         height 20         film_x 180        film_y 100        tile_end
tile_begin 315        x 180        y 220        z 2          width 20         height 20         film_x 260        film_y 80         tile_end
tile_begin 316        x 200        y 220        z 2          width 20         height 20         film_x 240        film_y 100        tile_end
tile_begin 317        x 220        y 220        z 2          width 20         height 20         film_x 200        film_y 60         tile_end
tile_begin 318        x 240        y 220        z 2          width 20         height 20         film_x 200        film_y 60         tile_end
tile_begin 319        x 260        y 220        z 2          width 20         height 20         film_x 200        film_y 60         tile_end
tile_begin 320        x 280        y 220        z 2          width 20         height 20         film_x 200        film_y 60         tile_end
tile_begin 321        x 300        y 220        z 2          width 20         height 20         film_x 200        film_y 60         tile_end
tile_begin 322        x 60         y 240        z 2          width 20         height 20         film_x 240        film_y 20         tile_end
tile_begin 323        x 140        y 240        z 2          width 20         height 20         film_x 280        film_y 0          tile_end
tile_begin 324        x 180        y 240        z 2          width 20         height 20         film_x 180        film_y 100        tile_end
tile_begin 325        x 200        y 240        z 2          width 20         height 20         film_x 200        film_y 100        tile_end
tile_begin 326        x 220        y 240        z 2          width 20         height 20         film_x 200        film_y 100        tile_end
tile_begin 327        x 240        y 240        z 2          width 20         height 20         film_x 200        film_y 100        tile_end
tile_begin 328        x 260        y 240        z 2          width 20         height 20         film_x 200        film_y 100        tile_end
tile_begin 329        x 280        y 240        z 2          width 20         height 20         film_x 200        film_y 100        tile_end
tile_begin 330        x 300        y 240        z 2          width 20         height 20         film_x 200        film_y 100        tile_end
tile_begin 331        x 20         y 260        z 2          width 20         height 20         film_x 220        film_y 120        tile_end
tile_begin 332        x 60         y 260        z 2          width 20         height 20         film_x 240        film_y 20         tile_end
tile_begin 333        x 140        y 260        z 2          width 20         height 20         film_x 280        film_y 20         tile_end
tile_begin 334        x 220        y 260        z 2          width 20         height 20         film_x 200        film_y 120        tile_end
tile_begin 335        x 240        y 260        z 2          width 20         height 20         film_x 280        film_y 140        tile_end
tile_begin 336        x 260        y 260        z 2          width 20         height 20         film_x 300        film_y 140        tile_end
tile_begin 337        x 0          y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 338        x 20         y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 339        x 40         y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 340        x 60         y 280        z 2          width 20         height 20         film_x 240        film_y 20         tile_end
tile_begin 341        x 80         y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 342        x 100        y 280        z 2          width 20         height 20         film_x 120        film_y 60         tile_end
tile_begin 343        x 120        y 280        z 2          width 20         height 20         film_x 140        film_y 60         tile_end
tile_begin 344        x 140        y 280        z 2          width 20         height 20         film_x 280        film_y 20         tile_end
tile_begin 345        x 160        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 346        x 180        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 347        x 200        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 348        x 220        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 349        x 240        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 350        x 260        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 351        x 280        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 352        x 300        y 280        z 2          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 353        x 0          y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 354        x 20         y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 355        x 40         y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 356        x 60         y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 357        x 80         y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 358        x 100        y 300        z 2          width 20         height 20         film_x 120        film_y 80         tile_end
tile_begin 359        x 120        y 300        z 2          width 20         height 20         film_x 140        film_y 80         tile_end
tile_begin 360        x 140        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 361        x 160        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 362        x 180        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 363        x 200        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 364        x 220        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 365        x 240        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 366        x 260        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 367        x 280        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 368        x 300        y 300        z 2          width 20         height 20         film_x 280        film_y 80         tile_end
tile_begin 369        x 60         y 280        z 3          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 370        x 140        y 280        z 3          width 20         height 20         film_x 280        film_y 60         tile_end
tile_begin 371        x 160        y 280        z 3          width 20         height 20         film_x 280        film_y 60         tile_end


Layer Above
tile_begin 372        x 40         y 100        z 4          width 20         height 20         film_x 280        film_y 120        position above      tile_end
tile_begin 373        x 60         y 100        z 4          width 20         height 20         film_x 300        film_y 120        position above      tile_end
tile_begin 374        x 240        y 240        z 4          width 20         height 20         film_x 280        film_y 120        position above      tile_end
tile_begin 375        x 260        y 240        z 4          width 20         height 20         film_x 300        film_y 120        position above      tile_end


Layer Collision
tile_begin 376        x 0          y 0          z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 377        x 0          y 20         z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 378        x 0          y 40         z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 379        x 220        y 40         z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 380        x 0          y 60         z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 381        x 60         y 60         z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 382        x 0          y 80         z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 383        x 0          y 100        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 384        x 20         y 100        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 385        x 140        y 100        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 386        x 160        y 100        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 387        x 0          y 120        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 388        x 40         y 120        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 389        x 60         y 120        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 390        x 140        y 120        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 391        x 160        y 120        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 392        x 0          y 140        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 393        x 80         y 140        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 394        x 0          y 160        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 395        x 300        y 160        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 396        x 0          y 180        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 397        x 120        y 180        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 398        x 240        y 180        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 399        x 0          y 200        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 400        x 0          y 220        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 401        x 0          y 240        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 402        x 0          y 260        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 403        x 20         y 260        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 404        x 220        y 260        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 405        x 240        y 260        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 406        x 260        y 260        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 407        x 0          y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 408        x 20         y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 409        x 40         y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 410        x 60         y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 411        x 80         y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 412        x 100        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 413        x 120        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 414        x 140        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 415        x 160        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 416        x 180        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 417        x 200        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 418        x 220        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 419        x 240        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 420        x 260        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 421        x 280        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 422        x 300        y 280        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 423        x 0          y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 424        x 20         y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 425        x 40         y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 426        x 60         y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 427        x 80         y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 428        x 100        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 429        x 120        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 430        x 140        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 431        x 160        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 432        x 180        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 433        x 200        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 434        x 220        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 435        x 240        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 436        x 260        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 437        x 280        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end
tile_begin 438        x 300        y 300        z 5          width 20         height 20         film_x 0          film_y 0          solid true       tile_end


Layer Warps
object_begin 439      x 300        y 20         z 6          width 20         height 260        warp_to_map SantaHQOutsideHome      warp_to_x -20        warp_to_y 20         transition east       object_end
object_begin 440      x 20         y 0          z 6          width 280        height 20         warp_to_map SantaHQOutsideStable      warp_to_x -20        warp_to_y 280        transition north      object_end
