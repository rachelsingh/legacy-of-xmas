package com.moosader.userinterface;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moosader.managers.TextureManager;
import com.moosader.screenhandle.DisplayManager;

public class InGameHudManager {
	protected Sprite m_leftSidebar;
	protected Sprite m_rightSidebar;
	
	protected ArrayList<Sprite> m_lstHPSprites;
	
	public InGameHudManager() {
		m_leftSidebar = new Sprite(new TextureRegion(TextureManager.ui_sidebars, 0, 0, 80, 320));
		m_leftSidebar.setPosition(0, 0);
		m_rightSidebar = new Sprite(new TextureRegion(TextureManager.ui_sidebars, 80, 0, 80, 320));
		m_rightSidebar.setPosition(480-80, 0);
		
		m_lstHPSprites = new ArrayList<Sprite>();
		m_lstHPSprites.add(new Sprite( new TextureRegion( TextureManager.ui_hudIcons, 0, 0, 15, 20 )));
		m_lstHPSprites.add(new Sprite( new TextureRegion( TextureManager.ui_hudIcons, 0, 0, 15, 20 )));
		m_lstHPSprites.add(new Sprite( new TextureRegion( TextureManager.ui_hudIcons, 0, 0, 15, 20 )));
		m_lstHPSprites.add(new Sprite( new TextureRegion( TextureManager.ui_hudIcons, 0, 0, 15, 20 )));
		for ( int i = 0; i < m_lstHPSprites.size(); i++ ) {
			m_lstHPSprites.get(i).setPosition(i * 15 + 5, 220);
		}
	}
	
	public void pushSprites(int playerHP) {
		DisplayManager.pushUiSprite(m_leftSidebar);
		DisplayManager.pushUiSprite(m_rightSidebar);
		for ( int i = 0; i < m_lstHPSprites.size(); i++ ) {
			if ( i * 25 <= playerHP ) {
				DisplayManager.pushUiSprite( m_lstHPSprites.get(i) );
			}
		}
	}
}
