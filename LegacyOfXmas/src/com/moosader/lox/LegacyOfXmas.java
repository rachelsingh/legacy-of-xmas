package com.moosader.lox;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.moosader.logging.Logger;
import com.moosader.managers.StateManager;
import com.moosader.states.GameState;

public class LegacyOfXmas implements ApplicationListener {
	private StateManager m_stateMgr;
	
	@Override
	public void create() {
		Logger.setupLogging( "Log.txt" );
		m_stateMgr = new StateManager();
		m_stateMgr.pushState( "gamestate", new GameState() );
		m_stateMgr.setCurrentState( "gamestate" );
	}

	@Override
	public void render() {	
		m_stateMgr.render(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void resize(int width, int height) {
		m_stateMgr.handleResize(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		Logger.closeLogging();
	}
}
