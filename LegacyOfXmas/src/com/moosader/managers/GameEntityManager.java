package com.moosader.managers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.moosader.entities.CharacterEntity;
import com.moosader.entities.IBaseObject;
import com.moosader.entities.Npc;
import com.moosader.entities.Player;
import com.moosader.entities.Weapon;
import com.moosader.screenhandle.DisplayManager;
import com.moosader.utilities.BoxCollision;

public class GameEntityManager extends EntityManager {

	public void update() {
		super.update();
		checkForAttack();	
	}
	
	protected void checkForAttack() {	
		ArrayList<Npc> npcs = new ArrayList<Npc>();
		ArrayList<Player> players = new ArrayList<Player>();
		
		Iterator it = m_lstEntities.entrySet().iterator();
		while( it.hasNext() ) {
			Map.Entry pairs = (Map.Entry)it.next();
			if ( ((IBaseObject) pairs.getValue()).isPlayerControlled() == false ) {
				npcs.add( (Npc) pairs.getValue() );
			}
			else {
				players.add( (Player) pairs.getValue() );				
			}
		}
		
		for ( Npc npc : npcs ) { 
			for ( Player player : players ) {
				Weapon weaponRef = player.getWeapon();
				if ( player.isAttacking() && 
						BoxCollision.isCollision(weaponRef.getCoords(), weaponRef.getDimens(), npc.getCoords(), npc.getDimens()) ) 
				{
					npc.handleDamage( player.getDamageRating() );			
				}

				if ( BoxCollision.isCollision( player.getCoords(), player.getDimens(), npc.getCoords(), npc.getDimens() ) ) {
					player.handleDamage( npc.getDamageRating() );
				}
			}
		}
	}
	
	public void pushSprites() {
		Iterator it = m_lstEntities.entrySet().iterator();
		while( it.hasNext() ) {
			Map.Entry pairs = (Map.Entry)it.next();
			CharacterEntity object = (CharacterEntity) pairs.getValue();
			
			if ( object.getHP() > 0 ) {
				if ( ((CharacterEntity) pairs.getValue()).isPlayerControlled() ) {
					Player playerObj = (Player) object;
					
					DisplayManager.pushGameSprite(object.getSprite());
					Sprite accessory = playerObj.getAccessorySprite();
					if ( accessory != null ) {
						DisplayManager.pushGameSprite( accessory );
					}	
				}
				else {
					Npc npcObj = (Npc) object;
					DisplayManager.pushGameSprite( npcObj.getSprite() );					
				}
			}
		}
	}
}
