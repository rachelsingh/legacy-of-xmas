package com.moosader.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.moosader.entities.Player;
import com.moosader.managers.EntityManager;
import com.moosader.managers.GameEntityManager;
import com.moosader.managers.MapManager;
import com.moosader.managers.TextureManager;
import com.moosader.screenhandle.DebugDisplay;
import com.moosader.screenhandle.DisplayManager;
import com.moosader.userinterface.InGameHudManager;
import com.moosader.utilities.GlobalOptions;

public class GameState extends IBaseState {
	
	private OrthographicCamera m_camera;
	private SpriteBatch m_batch;
	
	private GameEntityManager m_entityManager;
	private MapManager m_mapManager;
	private InGameHudManager m_hudManager;
	
	public GameState() {
		create();
	}
	
	@Override
	public void create() {
		int w = Gdx.graphics.getWidth();
		int h = Gdx.graphics.getHeight();
		
		m_camera = new OrthographicCamera();
		m_camera.setToOrtho(false, w, h);
		m_batch = new SpriteBatch();
		
		TextureManager.setup();
		DisplayManager.setup(480, 320, m_camera);
		DebugDisplay.setup();
		m_mapManager = new MapManager();
		m_mapManager.setup();
		
		m_entityManager = new GameEntityManager();
		m_entityManager.setup();
		m_entityManager.setSolidTiles(m_mapManager.getListOfSolidTiles());
	
		// TODO: Temporary while refactoring
		Player player = new Player(TextureManager.character_santa, new Vector2( 20, 20 ));
		player.setPosition(new Vector2(60, 60));
		
		m_entityManager.addPlayer("player", player);
		
		
		m_mapManager.bind(m_entityManager);
		
		m_hudManager = new InGameHudManager();
	}
	
	@Override
	public void update(float delta) {
		render(delta);
	}
	
	@Override
	public void render(float delta) {	
		// TODO: Cleanup
		m_entityManager.update();
		GlobalOptions.checkInput();
		m_mapManager.update();
		m_mapManager.pushBelowTiles();
		m_entityManager.pushSprites();
		m_hudManager.pushSprites(m_entityManager.getPlayer("player").getHP());
		m_mapManager.pushAboveTiles();
		
		if ( GlobalOptions.DebugMode ) {
			m_mapManager.pushDebugRegions();
		}
		m_mapManager.checkMapWarp(m_entityManager.getPlayer("player"));
		
		Gdx.gl.glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
				
		m_batch.setProjectionMatrix(m_camera.combined);
		m_batch.begin();
			DisplayManager.drawSprites(m_batch, m_camera);		
		m_batch.end();
			
		DebugDisplay.drawBorders();
	}

	@Override
	public void resize(int width, int height) {	
		DisplayManager.handleResize();	
	}

	@Override
	public void show() {		
	}

	@Override
	public void hide() {		
	}

	@Override
	public void pause() {		
	}

	@Override
	public void resume() {		
	}

	@Override
	public void dispose() {	
		m_batch.dispose();	
	}

	@Override
	public void handleResize(int width, int height) {
		DisplayManager.handleResize();
	}

	@Override
	public String nextState() {
		// TODO Auto-generated method stub
		return null;
	}
}
